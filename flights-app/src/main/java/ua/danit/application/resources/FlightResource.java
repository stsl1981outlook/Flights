package ua.danit.application.resources;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.codehaus.jackson.map.ObjectMapper;
import ua.danit.application.dao.FlightDao;
import ua.danit.application.model.Flight;

public class FlightResource {

  private FlightDao flightDao;

//  public FlightResource(FlightDao flightDao) {
//    this.flightDao = flightDao;
//  }

  @Path("/flights/top")
  @GET
  public String getTopFlights() throws IOException {
    if (flightDao == null) {
      flightDao = new FlightDao();
    }

    Iterable<Flight> flights = flightDao.getTopFlight(5);
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(flights);
  }

  @Path("/flights/search")
  @GET
  public String getSearchFlights(@QueryParam("search") String search) throws IOException {
    if (flightDao == null) {
      flightDao = new FlightDao();
    }

    Iterable<Flight> flights = flightDao.searchFlights(search);
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(flights);
  }

  //TODO: add new Path for Get By Id
}
