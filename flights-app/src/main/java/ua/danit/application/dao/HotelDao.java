package ua.danit.application.dao;


import ua.danit.application.model.Hotel;
import ua.danit.framework.utils.EntityUtils;

public class HotelDao {
    public Iterable<Hotel> getTopHotel(int limit) {
        String sql = "select ID as id, " +
                "`HOTELNAME` as `hotelName`, " +
                "`HOTELCITY` as `hotelCity`,"+
                "`HOTELSTARRATING` as `hotelStarRating` "+
                "from HOTELS";
        return EntityUtils.nativeQuery(sql, Hotel.class);
    }
    public Iterable<Hotel> searchHotels(String str) {
        String sql ="select ID as id, " +
                "`HOTELNAME` as `hotel`, " +
                "`HOTELCITY` as `city`,"+
                "`HOTELSTARRATING` as `stars` "+
                "from HOTELS " +
                "where `HOTELNAME` like'" + str + "%' ";

        return EntityUtils.nativeQuery(sql, Hotel.class);
    }
}
