package ua.danit.framework.utils;

import com.google.common.collect.Lists;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.List;

public class EntityUtils {

  public static  <T> List<T> nativeQuery(String sql, Class<T> clazz) {
    List<T> result = Lists.newArrayList();


    String PASSWD = "danit1";
    String DB_URL = "jdbc:mysql://mysql8.db4free.net:3307/danit_flights?useSSL=false";
    String USER = "danit1";

    try {
      Connection con = DriverManager.getConnection(DB_URL, USER, PASSWD);
      Statement statement = con.createStatement();
      ResultSet resultSet = statement.executeQuery(sql);
      ResultSetMetaData metaData = resultSet.getMetaData();

      while (resultSet.next()) {

        T flight1 = getInstance(clazz, resultSet, metaData);
        result.add(flight1);
      }

      resultSet.close();
      statement.close();
      con.close();


    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return result;
  }

  public static  <T> T getInstance(Class<T> clazz, ResultSet resultSet, ResultSetMetaData metaData) throws InstantiationException, IllegalAccessException, SQLException, NoSuchFieldException {
    T instance = clazz.newInstance();
    int columnCount = metaData.getColumnCount();

    for (int i = 1; i <= columnCount; i++) {

      String columnLabel = metaData.getColumnLabel(i);
      Field field = clazz.getDeclaredField(columnLabel);
      Object values = resultSet.getObject(i, field.getType());

      field.setAccessible(true);
      field.set(instance, values);
    }

    return instance;
  }
}
